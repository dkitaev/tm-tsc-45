package ru.tsc.kitaev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.dto.SessionDTO;
import ru.tsc.kitaev.tm.dto.UserDTO;

import java.util.List;

public interface ISessionDTOService {

    @NotNull
    SessionDTO open(@Nullable String login, @Nullable String password);

    @Nullable
    UserDTO checkDataAccess(@Nullable String login, @Nullable String password);

    void validate(@Nullable SessionDTO session, Role role);

    void validate(@Nullable SessionDTO session);

    @NotNull
    SessionDTO sign(@NotNull SessionDTO session);

    void close(@Nullable SessionDTO session);

}
