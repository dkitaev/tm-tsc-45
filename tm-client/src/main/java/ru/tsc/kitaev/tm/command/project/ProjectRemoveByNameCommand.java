package ru.tsc.kitaev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.command.AbstractProjectCommand;
import ru.tsc.kitaev.tm.endpoint.SessionDTO;
import ru.tsc.kitaev.tm.util.TerminalUtil;

public final class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by name...";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getSession();
        System.out.println("Enter Name");
        @NotNull final String name = TerminalUtil.nextLine();
        serviceLocator.getProjectEndpoint().removeProjectByName(session, name);
        serviceLocator.getProjectTaskEndpoint().removeByName(session, name);
        System.out.println("[OK]");
    }

}
